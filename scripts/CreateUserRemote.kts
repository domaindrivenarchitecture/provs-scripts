import io.provs.Secret
import io.provs.remote
import io.provs.ubuntu.secret.secretSources.PromptSecretSource
import io.provs.ubuntu.user.base.createUser
import kotlin.system.exitProcess

if (args.size != 2 || args[0].isEmpty() || args[1].isEmpty()) {
    println("MISSING PARAMETERS - Pls provide host and remote user as parameters, e.g.: 192.162.56.123 testuser")
    exitProcess(1)
}


val remotePassword = PromptSecretSource("Password for " + args[1] + " on " + args[0]).secret()

remote(args[0], args[1], remotePassword).createUser("testuser", password = Secret("test1234"), sudo = true)
