import io.provs.Prov
import io.provs.ProvResult
import io.provs.remote
import io.provs.ubuntu.secret.secretSources.PromptSecretSource
import kotlin.system.exitProcess

if (args.size != 2 || args[0].isEmpty() || args[1].isEmpty()) {
    println("MISSING PARAMETERS - Pls provide host and remote user as parameters, e.g.: 192.162.56.123 testuser")
    exitProcess(1)
}


val remotePassword = PromptSecretSource("Password for " + args[1] + " on " + args[0]).secret()

remote(args[0], args[1], remotePassword).printInfos().out


fun Prov.printInfos() = def {
    // os version
    val ubuntuVersion = cmd("lsb_release -a").out
    println("\nUbuntu Version:\n${ubuntuVersion}")

    // current dir
    val currentDir = cmd("pwd").out
    println("Current directory:\n${currentDir}")

    // time zone
    val timeZone = cmd("cat /etc/timezone").out
    println("Time zone:\n${timeZone}")

    ProvResult(true)
}

