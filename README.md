
This repo is part of the [provs framework](https://gitlab.com/domaindrivenarchitecture/provs-docs). It provides scripts ready-to-use for provisioning or which can be used as a base for  own scripts.
An executable jar-file of the provs-framework is included in this repo.

## Compatibility

This repository is meant to be used on **Ubuntu**, but the examples should (mostly) also work on other *nix systems. 

Table of Contents
=================

* [Getting started](#getting-started)
    * [Prerequisites](#prerequisites)
    * [Usage](#usage)
    * [Hello world](#hello-world)
* [Script examples remote machines](#script-examples-remote-machines)
    * [Ssh prerequisites](#ssh-prerequisites)
    * [Show remote system infos](#show-remote-system-infos)
    * [Provision a complete workplace on a remote machine](#provision-a-complete-workplace-on-a-remote-machine)
    * [Provision nginx on a remote machine](#provision-nginx-on-a-remote-machine)
* [For developers - Build your own scripts](#for-developers---build-your-own-scripts)
    * [Define your own functions](#define-your-own-functions)
    * [Nested functions](#nested-functions)


    
# Getting started
## Prerequisites

1. [Kotlin](https://kotlinlang.org/docs/tutorials/command-line.html)
1. Clone this repo
2. Add command **provs** to your shell by executing the following command in the local repo's root folder:  
`. defprovs` or `source defprovs`
   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](documentation/images/screenshot_defprovs.png)

**Note:** the required jar-file for running the scripts is already included in this repo.  
If you'd rather want to build it yourself, then just clone the git repo [provs-ubuntu-extensions](https://gitlab.com/domaindrivenarchitecture/provs-ubuntu-extensions) and build the jar-file by running `gradlew fatJarLatest`. You'll find it in `build/libs/provs-extensions.jar`.

## Usage

Run a script by the following command in the project root:

`provs <script-name>`

(This is the short version of:
`kotlinc -cp provs-extensions.jar -script scripts/ScriptName.kts`)

# Running locally

## Hello world

`provs HelloWorld.kts`

Just does what it is supposed to do: runs and prints "Hello world".

![](documentation/images/screenshot-hello-world.png)


# Running in docker

If you have docker installed, you can run hello-world in a docker container:

`provs HelloWorldDocker.kts`

As shown in this script: the function `docker()` allows you to run provs-code in a docker container.  
If no container-name resp. image is specified, a default container of image "ubuntu" will be used.  

# Script examples remote machines

## Ssh prerequisites

To run scripts against a remote system you need a running ssh-server on the remote machine, 
e.g. by installing [openssh-server](https://ubuntu.com/server/docs/service-openssh).
The examples below are based on password authentication for the remote user.

Hint: Alternatively you could also use **ssh key-based authentication** instead 
by removing the password from the remote function, i.e. in the corresponding script use just:  
`remote(args[0], args[1])...` instead of `remote(args[0], args[1], PromptSecretSource().secret())...` 


## Show remote system infos 

Run the script `SystemInfosRemote.kts` with the **host-ip** and **username** as parameters and you'll be prompted for the password of the remote user:

`provs SystemInfosRemote.kts <host> <remote user>`

The script prints some infos about the remote system.

![](documentation/images/screenshot-system-infos2.png)

## Provision a complete workplace on a remote machine

The default script provisions just a minimal workplace config with installing some productivity tools, git and some more.
For type OFFICE (with Thunderbird, LibreOffice, etc) or IDE (contains developer tooling) pls specify the corresponding WorkplaceType in the script.

`provs WorkplaceRemote.kts <host> <remote user>`

![](documentation/images/screenshot_workplace_min.png)

This installs nginx with a default config. For a custom config, change the script to pass your own config to method `provisionNginxStandAlone()`

## Provision nginx on a remote machine

Provision nginx on a remote machine with  
`provs NginxRemote.kts <host> <remote user>`

![](documentation/images/screenshot_nginx.png)

This installs nginx with a default config. For a custom config, change the script to pass your own config to method `provisionNginxStandAlone()`


# For developers - Build your own scripts

Build your own script with
1. Create a KotlinScript file with ".kts" extension (e.g. `MyScript.kts`) in folder `scripts` of this repo.
2. In this file
    * add the required imports, minimal `import io.provs.*`
    * add your code, e.g. if you would want to get information about available disk space with the `df -h` command,
      you'd just want to execute a single command and print its results:
   ```kotlin
   import io.provs.*
   var diskfree = local().cmd("df -h").out
   println(diskfree)
   ```
1. Run it with `provs MyScript.kts` in the root folder of the repo.
4. To run the same on a remote machine just replace `local()` with
    * `remote("ip-or-hostname", "username", "password")` for authentication with user-name and password or with
    * `remote("ip-or-hostname", "username")` for authentication with ssh key.

To execute multiple commands you can use something like:
   ```kotlin
   import io.provs.*
   local().sh("""
   mkdir tmp
   echo Hello provs! > tmp/tmpfile.txt
   """)
   ```

Run it and get a hopefully similar result as:

![](documentation/images/screenshot_myscript.png)

As described above you can easily run your script remotely.

**Limitations**:
* If using the `sh`-function each line is executed separately, i.e. you cannot switch to a folder and use it in the next line as in:
   ```kotlin
   sh("""
   cd tmp
   echo Hello provs! > tmpfile.txt # !!! this will not create the file in folder "tmp" but in the home folder
   """)
   ```
  In these cases you may use e.g. the following instead:
   ```kotlin
   sh("""
   cd tmp && echo Hello provs! > tmpfile.txt
   """)
   ```

## Define your own functions

You can easily define your own function with:

```kotlin
import io.provs.*
fun Prov.myFunction() = def {
   // your logic goes here ...
}
local().myFunction()   // run locally
```

The function body can contain calls to other Provs functions as well as custom Kotlin code.

**Be aware,** that the last value in your function needs to be of type ProvResult, which will define the outcome of your function.
Or just use some other Prov function as the last statement as it will return a ProvResult.

So, either provs command like `cmd("somecommand")` or an explicitly  `ProvResult(true)` would do.

And remember: To run it remotely just replace `local()` with e.g. `remote("ip-or-hostname", "username", "password")`

## Nested functions

If you want to use a bunch of code lines or you want to modularize for other reasons, just combine your functions like

```kotlin
import io.provs.*
fun Prov.makeMyDir() = def {
   cmd("mkdir tmp123")
}
fun Prov.makeMyFile() = def {
   cmd("echo awesome > tmp123/myfile.txt")
}
fun Prov.myConfig() = def {
   makeMyDir()
   makeMyFile()
}
local().myConfig()
```

For demo reasons the examples above use just simple shell commands.
Of course, in real life it would make sense to use the built-in file functions of provs-ubuntu 
like `createFile()`, `createDir()`, etc, which also have additional advantages.
